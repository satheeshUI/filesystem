import Vue from 'vue'
import Router from 'vue-router'
import FileSystem from '@/modules/FileSystem'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'FileSystem',
      component: FileSystem
    }
  ]
})
