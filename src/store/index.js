import Vue from 'vue'
import Vuex from 'vuex'
import mutations from './mutation'
import actions from './actions'

Vue.use(Vuex)

const store = new Vuex.Store({
  strict: false,
  state: {
    filesData: null
  },
  mutations,
  actions
})

export default store
