export default {
  GET_FILE_ITEMS (state, payload) {
    let output = []
    let term = [ {arr: output, id: '0'} ]
    for (let el of payload) {
      el.showSub = false
      if (el.parent_folder_id === 'null') {
        el.parent_folder_id = '0'
      }
      let termIndex = term.findIndex(t => t.id == el.parent_folder_id)
      if (!Array.isArray(term[termIndex].arr)) {
        term[termIndex].arr = term[termIndex].arr.children = []
      }
      let currentTerm = Object.assign({}, el)
      term[termIndex].arr.push(currentTerm)
      term[++termIndex] = { arr: currentTerm, id: el.folder_id }
    }
    state.filesData = output
  }
}
