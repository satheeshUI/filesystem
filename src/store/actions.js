import Vue from 'vue'

export default {
  getAPIRequest ({commit}, payload) {
    Vue.http.get('./static/mock/files.json').then(res => {
      commit('GET_FILE_ITEMS', res.data)
    })
  }
}
